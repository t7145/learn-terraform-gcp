output "web1-hostname" {
  value = google_compute_instance.web_private_1.name
}

output "web1-internal-ip" {
  value = google_compute_instance.web_private_1.network_interface.0.network_ip
}

output "web2-hostname" {
  value = google_compute_instance.web_private_2.name
}

output "web2-internal-ip" {
  value = google_compute_instance.web_private_2.network_interface.0.network_ip
}

output "nat_ip_address" {
  value = google_compute_address.nat-ip.address
}

# show external ip address of load balancer

output "load-balancer-ip-address" {
  value = google_compute_global_forwarding_rule.global_forwarding_rule.ip_address
}
