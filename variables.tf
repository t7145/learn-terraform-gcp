variable "project" {
  description = "Project ID"
  type        = string
}

variable "credentials_file" {
  description = "Credentials File"
  type        = string
}

variable "region" {
  description = "GCP region"
  type        = string
}

variable "zone" {
  description = "GCP zone"
  type        = string
}

variable "private_subnet_cidr_1" {
  description = "Private subnet CIDR 1"
  type        = string
}

variable "app_name" {
  description = "Application name"
  type        = string
}

variable "app_domain" {
  description = "Domain"
  type        = string
}